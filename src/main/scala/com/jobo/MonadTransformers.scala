package com.jobo

import cats.data.EitherT

import scala.concurrent.Future

object MonadTransformers {
  import cats.data.OptionT

  type ListOption[A] = OptionT[List, A]

  import cats.Monad
  import cats.instances.list._
  import cats.syntax.applicative._

  val result: ListOption[Int] = 43.pure[ListOption]

  val a: ListOption[Int] = 10.pure[ListOption]
  val b: ListOption[Int] = 32.pure[ListOption]

  a flatMap { (x: Int) =>
    b map { (y: Int) =>
      x + y
    }
  }


  import cats.instances.either._
  import cats.syntax.applicative._

  type Error = String

  type EitherOr[A] = Either[Error,A]

  type EitherOptionOr[A] = OptionT[EitherOr,A]

  23.pure[EitherOptionOr]

  import cats.data.EitherT
  import cats.instances.all._
  import scala.concurrent.Future
  import scala.concurrent.ExecutionContext.Implicits.global

  type FutureEither[A] = EitherT[Future,Error,A]
  type FutureEitherOption[A] = OptionT[FutureEither,A]

  val ans: FutureEitherOption[Int] =
    for {
      a <- 10.pure[FutureEitherOption]
      b <- 20.pure[FutureEitherOption]
    } yield a + b

  // Kind Projector
//  import cats.instances.option._
//  val res = 123.pure[EitherT[Option,String,?]]
//  println(res)
}

object TransformAndRollOut {
  import cats.instances.all._
  import scala.concurrent.ExecutionContext.Implicits.global

  type Response[A] = EitherT[Future, String, A]

  val powerLevels = Map(
    "Jazz" -> 6,
    "Bumblebee" -> 8,
    "Hot Rod" -> 10
  )

  def getPowerLevel(autobot: String): Response[Int] = {
    val ans = powerLevels.get(autobot) match {
      case None => Left("Can't find power level for provided autobot name: " + autobot)
      case Some(power) => Right(power)
    }
    EitherT(Future.successful(ans))

    // Or EitherT.right(Future(avg))
  }

  def canSpecialMove(ally1: String, ally2: String): Response[Boolean] =
    for {
      a <- getPowerLevel(ally1)
      b <- getPowerLevel(ally2)
    } yield (a + b) > 15

  import scala.concurrent.ExecutionContext.Implicits.global
  import scala.concurrent.Await
  import scala.concurrent.duration._

  def tacticalReport(ally1: String, ally2: String): String = {
    Await.result(canSpecialMove(ally1, ally2).value, 1 second) match {
      case Left(msg) =>
        s"Comms error: $msg"
      case Right(true) =>
        s"$ally1 and $ally2 are ready to roll out!"
      case Right(false) =>
        s"$ally1 and $ally2 need a recharge."
    }

  }
}