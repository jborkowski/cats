package com.jobo

object Calc {
  import cats.data.State
  import cats.syntax.applicative._

  type CalcState[A] = State[List[Int], A]

  def evalOne(sym: String): CalcState[Int] =
    sym match {
      case "+" => operator(_ + _)
      case "-" => operator(_ - _)
      case "*" => operator(_ * _)
      case "/" => operator(_ / _)
      case num => operand(num.toInt)
    }

  def evalAll(input: List[String]): CalcState[Int] =
    input.foldLeft(0.pure[CalcState]) { (state, sym) =>
      state flatMap (_ => evalOne(sym)) // != evalOne(sym)
    }

  def operand(num: Int): CalcState[Int] = State[List[Int], Int] { oldState =>
    (num :: oldState, num)
  }

  def operator(f: (Int, Int) => Int): CalcState[Int] = State[List[Int], Int] {
    case x :: y :: tail =>
      val res = f(x, y)
      (res :: tail, res)
    case _ =>
      sys.error("Something went wrong!")
  }
}

object FoldStateTest {
  import cats.data.State
  import cats.syntax.applicative._

  type Stack[A] = State[List[Int], A]

  val in = List(1, 2, 3, 4, 5)

  val ans = in.foldLeft(0.pure[Stack]) { (stack, value) =>
    stack flatMap (o => (o + value).pure[Stack])
  }
}


object TestAppCalc extends App {
  val toCalc = List("1", "2", "+", "3", "*")

  import Calc._

  val res = evalAll(toCalc).runA(Nil).value

  println(res)

  val program = for {
    _ <- evalAll(List("1", "2", "+"))
    _ <- evalAll(List("3", "4", "+"))
    ans <- evalOne("*")
  } yield ans

  println(program.runA(Nil).value)

  import FoldStateTest._
  println(ans.run(Nil).value)
}