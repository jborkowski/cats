package com.jobo

sealed trait Tree[+A]
final case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]
final case class Leaf[A](value: A) extends Tree[A]

final case class Box[A](value: A)

object Functors {
  import cats.Functor

  implicit def treeFunctor = new Functor[Tree] {
    def map[A, B](fa: Tree[A])(f: (A) => B): Tree[B] = fa match {
      case Branch(l, r) => Branch(map(l)(f), map(r)(f))
      case Leaf(a) => Leaf(f(a))
    }
  }

  trait Printable[A] {
    def format(value: A): String
    def contamap[B](func: B => A): Printable[B] = {
      val self = this
      (value: B) => self.format(func(value))
    }
  }

  def format[A](value: A)(implicit p: Printable[A]): String =
    p.format(value)

  implicit val stringPrintable = new Printable[String] {
    def format(value: String): String = "\"" + value + "\""
  }

  implicit val booleanPrintable = new Printable[Boolean] {
    def format(bool: Boolean): String =
      if (bool) "yes" else "no"
  }


  import scala.language.higherKinds

  implicit def boxPrintable[A](implicit p: Printable[A]) =
    p.contamap[Box[A]](_.value)

  trait Codec[A] {
    def encode(value: A): String
    def decode(value: String): Option[A]

    def imap[B](dec: A => B, enc: B => A): Codec[B] ={
      val self = this
      new Codec[B] {
        def encode(value: B): String = self.encode(enc(value))
        def decode(value: String): Option[B] = self.decode(value).map(dec)
      }
    }
  }

  def encode[A](value: A)(implicit c: Codec[A]): String =
    c.encode(value)

  def decode[A](value: String)(implicit c: Codec[A]): Option[A] =
    c.decode(value)

  implicit val intCodec = new Codec[Int] {
    def encode(int: Int): String = int.toString
    def decode(txt: String): Option[Int] = scala.util.Try(txt.toInt).toOption
  }

  implicit def boxCodec[A](implicit c: Codec[A]) = c.imap[Box[A]](Box(_), _.value)
}

object EApp extends App {

  import Functors._
  import cats.Functor
  import cats.syntax.functor._

  val t: Tree[Int] = Branch(Leaf(1), Branch(Leaf(3), Leaf(4)))

  t.map(_ * 3) // syntax

  val s = Functor[Tree].map(t)(i => i * 3)
  println(s)

  println(format(Box("hello world")))


  println(encode(Box(123)))

  println(decode[Box[Int]]("123"))
}
