package com.jobo

import cats.data.Reader
import com.jobo.catss.`val`

object Readers extends App {
  import cats.syntax.applicative._
  type DbReader[A] = Reader[Db, A]

  def findUsername(userId: Int): DbReader[Option[String]] =
    Reader(_.usernames.get(userId))

  def checkPassword(username: String, password: String): DbReader[Boolean] =
    Reader(_.passwords.get(username).forall(_ == password))

  def checkLogin(userId: Int, password: String): DbReader[Boolean] =
      for {
        u <- findUsername(userId)
        r <- u.map { username =>
          checkPassword(username, password)
        }.getOrElse(false.pure[DbReader])
      } yield r



  val db = Db(
    Map(
      1 -> "dade",
      2 -> "kate",
      3 -> "margo"
    ),
    Map(
      "dade" -> "zerocool",
      "kate" -> "acidburn",
      "margo" -> "secret"
    )
  )

  val isActive = checkLogin(1, "zerocool").run(db)
  val isActive1 = checkLogin(4, "davici").run(db)

  println(s"User1 is active: $isActive")
  println(s"User2 is active: $isActive1")
}

case class Db(usernames: Map[Int, String], passwords: Map[String, String])
