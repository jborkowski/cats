package com.jobo

import scala.concurrent.Future
import scala.language.higherKinds
import cats.{Applicative, Id}
import cats.instances.future._
import cats.instances.list._
import cats.syntax.traverse._
import cats.syntax.functor._
import com.jobo.catss.`val`

import scala.concurrent.ExecutionContext.Implicits.global

class UptimeService[M[_]](client: UptimeClient[M]) {
  def getTotalUptime(hostnames: List[String])(implicit a: Applicative[M]): M[Int] =
    hostnames.traverse(client.getUptime).map(_.sum)
}

// Abstracting over types...
trait UptimeClient[M[_]] {
  def getUptime(hostname: String): M[Int]
}

trait RealUptimeClient extends UptimeClient[Future] {
  def getUptime(hostname: String): Future[Int]
}

trait TestUptimeClient extends UptimeClient[Id] {
  def getUptime(hostname: String): Int
}

class TestUptimeClienta(hosts: Map[String, Int]) extends TestUptimeClient {
  def getUptime(hostname: String): Int =
    hosts.getOrElse(hostname, 0)
}

object TestingAbstraction extends App {
  val testTotalUptime = {
    val hosts     = Map("host1" -> 10, "host2" -> 6)
    val client    = new TestUptimeClienta(hosts)
    val service   = new UptimeService(client)
    val actual    = service.getTotalUptime(hosts.keys.toList)
    val expected  = hosts.values.sum

    assert(actual == expected)
    println("Test passed")
  }
}