package com.jobo

object MonoidImpl {
  // Monoid's two laws: identity and associativity
  trait Monoid[A] extends Semigroup[A] {
    def empty: A
  }

  trait Semigroup[A] {
    def combine(x: A, y: A): A
  }

  object Monoid {
    def apply[A](implicit monoid: Monoid[A]) = monoid
  }

  implicit val booleanAndMonoid = new Monoid[Boolean] {
    def empty: Boolean = true
    def combine(x: Boolean, y: Boolean): Boolean = x && y
  }

  implicit val booleanOrMonoid = new Monoid[Boolean] {
    def empty: Boolean = false
    def combine(x: Boolean, y: Boolean): Boolean = x || y
  }

  implicit val booleanEitherMonoid = new Monoid[Boolean] {
    def empty: Boolean = false
    def combine(x: Boolean, y: Boolean): Boolean =
      (x && !y) || (!x && y)
  }

  implicit val booleanXnorMonoid = new Monoid[Boolean] {
    def empty: Boolean = true
    def combine(x: Boolean, y: Boolean): Boolean =
      (x || !y) && (!x || y)
  }

  implicit def setUnionMonoid[A] = new Monoid[Set[A]] {
    def empty: Set[A] = Set.empty[A]
    def combine(x: Set[A], y: Set[A]): Set[A] =
      x union y //x.union(y)
  }

  implicit def setIntersectionMonoid[A] = new Monoid[Set[A]] {
    def empty: Set[A] = Set.empty[A]
    def combine(x: Set[A], y: Set[A]): Set[A] =
      x intersect y
  }
}

case class Order(totalCost: Double, quantity: Double)

object SupperAdder {
  import cats.Monoid
  import cats.syntax.monoid._

  implicit val orderMonoid = new Monoid[Order] {
    def empty: Order = Order(0, 0)
    def combine(x: Order, y: Order): Order =
      Order(x.totalCost + y.totalCost, x.quantity + y.quantity)
  }

  /*def add(items: List[Int]): Int =
    items.foldLeft(Monoid[Int].empty)(_ |+| _)*/

  def add[A: Monoid](items: List[A]): A =
    items.foldLeft(Monoid[A].empty)(_ |+| _)
}

object TestApp extends App {
  import cats.Monoid
  import cats.instances.string._

  val combined = Monoid[String].combine("Hi ", "there")
  println(combined)

  Monoid[String].empty // empty string

  Monoid.apply[String].combine("Hi ", "There") // we also can use...

  // Monoid extends Semigroup!
  import cats.Semigroup
  Semigroup[String].combine("Hi ", "there")

  import cats.instances.int._
  Monoid[Int].combine(12, 32)

  import cats.instances.int._
  import cats.instances.option._

  Monoid[Option[Int]].combine(Some(12), None) // Some(12)
  Monoid[Option[Int]].combine(Some(12), Some(43)) // Some(55)

  import cats.syntax.semigroup._
  import cats.instances.string._
  val res = "Hi " |+| "there."

  import cats.instances.int._
  1 |+| 2 |+| Monoid[Int].empty



  val result = SupperAdder.add(List(Some(1): Option[Int], Some(2): Option[Int]))
  println(result)

  println(T.mapR)

}


object T {
  import cats.Monoid
  import cats.instances.all._
  import cats.syntax.semigroup._
  val map1 = Map("a" -> Set(1), "b" -> Set(2))
  val map2 = Map("b" -> Set(3), "d" -> Set(4))

  val mapR = map1 |+| map2

}