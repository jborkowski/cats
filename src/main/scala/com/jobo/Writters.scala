package com.jobo

object Writers extends App {
  import scala.concurrent._
  import scala.concurrent.ExecutionContext.Implicits.global
  import scala.concurrent.duration._

  import FactorialLogged._

  val Vector((logA, ansA), (logB, ansB)) = Await.result(
    Future.sequence(
      Vector(
        Future(factorial(3).run),
        Future(factorial(3).run)
      )
    ),
    5.seconds
  )

  println(s"Answer $ansA + logs: $logA")
  println(s"Answer $ansB + logs: $logB")
}

object FactorialLogged {
  import cats.syntax.applicative._
  import cats.syntax.writer._
  import cats.data.Writer
  import cats.instances.vector._

  type Logged[A] = Writer[Vector[String],A]

  def factorial(n: Int): Logged[Int] =
    for {
      ans <- if (n == 1) {
        1.pure[Logged]
      } else {
        Factorial.slowly(factorial(n - 1).map(_ * n))
      }
      _ <- Vector(s"fact $n $ans").tell
    } yield ans
}

object Factorial {
  def slowly[A](body: => A) =
    try body finally Thread.sleep(100)

  def factorial(n: Int): Int = {
    val ans = slowly(if(n == 0) 1 else n * factorial(n - 1))
    println(s"fact $n $ans")
    ans
  }
}
