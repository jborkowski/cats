package com.jobo.catss.`val`

import cats.Semigroup
import cats.data.Validated
import cats.data.Validated._
import cats.syntax.cartesian._
import cats.syntax.semigroup._

sealed trait Predicate[E,A] {
  def or(that: Predicate[E,A]): Predicate[E,A] =
    Or(this, that)

  def and(that: Predicate[E,A]): Predicate[E,A] =
    And(this, that)

  def apply(a: A)(implicit s: Semigroup[E]): Validated[E,A] =
    this match {
//      case Pure(f) =>
//        f(a)
      case And(l, r) =>
        (l(a) |@| r(a)).map((_, _) => a)
      case Or(l, r) =>
        l(a) match {
          case Valid(a) =>
            Valid(a)
          case Invalid(e) =>
            r(a) match {
              case Valid(a) =>
                Valid(a)
              case Invalid(e1) =>
                Invalid(e |+| e1)
            }
        }
    }
}

final case class And[E,A](left: Predicate[E,A], right: Predicate[E,A]) extends Predicate[E,A]
final case class Or[E,A](left: Predicate[E,A], right: Predicate[E,A]) extends Predicate[E,A]
//final case class Pure[E,A](f: A => Validated[E,A]) extends Predicate[E,A]

object Check {
  def apply[E,A](p: Predicate[E,A]): Check[E,A,A] =
    Pure(p)
}

final case class Pure[E,A](p: Predicate[E,A]) extends Check[E,A,A] {
  override def apply(a: A)(implicit s: Semigroup[E]): Validated[E, A] =
    p(a)
}

final case class Map[E,A,B,C](check: Check[E,A,B], f: B => C) extends Check[E,A,C] {
  def apply(a: A)(implicit s: Semigroup[E]) =
    check(a).map(f)
}

final case class FlatMap[E,A,B,C](check: Check[E,A,B], f: B => Check[E,A,C]) extends Check[E,A,C] {
  def apply(a: A)(implicit s: Semigroup[E]): Validated[E,C] =
    check(a).withEither(_.flatMap(b => f(b)(a).toEither))
}

final case class AndThen[E,A,B,C](check1: Check[E,A,B], check2: Check[E,B,C]) extends Check[E,A,C] {
  def apply(a: A)(implicit s: Semigroup[E]): Validated[E,C] =
    check1(a).withEither(_.flatMap(b => check2(b).toEither))
}

sealed trait Check[E,A,B] {
  def apply(a: A)(implicit s: Semigroup[E]): Validated[E,B]

  def map[C](f: B => C): Check[E,A,C] =
    Map[E,A,B,C](this, f)

  def flatMap[C](f: B => Check[E,A,C]) =
    FlatMap[E,A,B,C](this, f)

  def andThen[C](that: Check[E,B,C]): Check[E,A,C] =
    AndThen[E,A,B,C](this, that)

}

object CheckTest extends App {

  def firstTest() = {
//    val a: Predicate[List[String], Int] =
//     Pure { v =>
//      if (v > 2) Valid(v)
//      else Invalid(List("Must be > 2"))
//     }
//
//    val b: Predicate[List[String], Int] =
//      Pure { v =>
//        if (v < -2) Valid(v)
//        else Invalid(List("Must be < -2"))
//      }
//
//    val c: Predicate[List[String], Int] =
//      Pure { v =>
//        if (v > 6) Valid(v)
//        else Invalid(List("Must be > 6"))
//      }
//
//    val check = a and b or c
//
//    println(check(5))
//    println(check(0))
//    println(check(7))
  }

  firstTest()
}