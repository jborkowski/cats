package com.jobo

import java.time.LocalDateTime

import scala.concurrent.{Await, Future, Promise}
import scala.util.Try

object CartesianAndApplicative {

}

object FutureTestApp extends App {
  import scala.concurrent.ExecutionContext.Implicits.global
  import scala.concurrent.duration._

  //blocked Future
  def time(a: => LocalDateTime): Future[LocalDateTime] = {
    Future {
      val start = a
      Thread.sleep(1000)
      start
    }
  }

  Await.result(Future.sequence(Seq.fill(3)(time(LocalDateTime.now))), 15 second).foreach(println)

  def delay(dur:Deadline) = {
    Try(Await.ready(Promise().future, dur.timeLeft))
  }

  import cats.Cartesian
  import cats.instances.future._
  import cats.syntax.cartesian._

  val futures = (
    time(LocalDateTime.now) |@|
    time(LocalDateTime.now) |@|
    time(LocalDateTime.now)
  ).map((t1, t2, t3) => {
    println(t1)
    println(t2)
    println(t3)
  } )


  Await.result(futures, 15 second)

}