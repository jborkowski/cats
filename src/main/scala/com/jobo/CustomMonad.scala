package com.jobo


class CustomMonad {
  import cats.{Monad => CatsMonad}
  import scala.annotation.tailrec

  implicit val optionMonad = new CatsMonad[Option] {
    def flatMap[A,B](opt: Option[A])(fn: A => Option[B]): Option[B] =
      opt flatMap fn
    def pure[A](opt: A): Option[A] =
      Some(opt)
    @tailrec
    def tailRecM[A,B](a: A)(fn: A => Option[Either[A,B]]): Option[B] =
      fn(a) match {
        case None           => None
        case Some(Left(a1)) => tailRecM(a1)(fn)
        case Some(Right(b)) => Some(b)
      }
  }
}

object CustomMonadForTree {
  import cats.{Monad => CatsMonad}
  def branch[A](left: Tree[A], right: Tree[A]): Tree[A] =
    Branch(left, right)

  def leaf[A](value: A): Tree[A] =
    Leaf(value)

  implicit val treeMonad = new CatsMonad[Tree] {
    def pure[A](x: A): Tree[A] =
      leaf(x)

    def flatMap[A, B](fa: Tree[A])(f: (A) => Tree[B]): Tree[B] = fa match {
      case Branch(l, r) => branch(flatMap(l)(f), flatMap(r)(f))
      case Leaf(v)      => f(v)
    }

    def tailRecM[A, B](a: A)(f: (A) => Tree[Either[A, B]]): Tree[B] =
      f(a) match {
        case Leaf(Left(a1)) => tailRecM(a1)(f)
        case Leaf(Right(b)) => leaf(b)
        case Branch(l, r)   =>
          branch(
            flatMap(l) {
              case Left(a1) => tailRecM(a1)(f)
              case Right(b) => pure(b)
            },
            flatMap(r) {
              case Left(a1) => tailRecM(a1)(f)
              case Right(b) => pure(b)
            }
          )
      }
  }
}

object TestAppCM extends App {
  import CustomMonadForTree._
  import cats.syntax.functor._
  import cats.syntax.flatMap._

  val treeTest = branch(leaf(100), leaf(999))

  treeTest.map(i => i * 3)

  val anstree = treeTest.flatMap(x => branch(leaf(x - 2), branch(leaf(x-1), leaf(x-9))))

  println(anstree)

  val res3 = for {
    a <- branch(leaf(90), leaf(23))
    b <- branch(branch(leaf(a * 123), leaf(a - 2)), leaf(a))
    c <- branch(leaf(b), branch(leaf(b), leaf(b + 3)))
  } yield c

  println(res3)
}
