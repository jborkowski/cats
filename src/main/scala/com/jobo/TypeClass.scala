package com.jobo


final case class Person(name: String, email: String)

final case class Cat(name: String, age: Int, color: String)

object Printable {
  def format[A](a: A)(implicit p: Printable[A]): String = p.format(a)
  def print[A](a: A)(implicit p: Printable[A]): Unit = println(format(a))
}

trait Printable[A] {
  def format(a: A): String
}

object PrintableInstances {
  implicit val stringPrintable = new Printable[String] {
    def format(a: String): String = a
  }

  implicit val intPrintable = new Printable[Int] {
    def format(a: Int): String = String.valueOf(a)
  }

  implicit val catPrintable = new Printable[Cat] {
    def format(cat: Cat): String = s"${cat.name} is a ${cat.age} year-old ${cat.color} cat."
  }
}

// Better Syntax!
object PrintableSyntax {
  implicit class PrintOps[A](value: A) {
    def format(implicit p: Printable[A]): String = p.format(value)
    def print(implicit p: Printable[A]): Unit = println(p.format(value))
  }
}

object TestApp2 extends App {
  import PrintableInstances._

  val cat = Cat("Anniozy", 31, "brown")

  Printable.print(cat)

  import PrintableSyntax._

  Cat("Mruu", 1, "black").print


  // Cats
  import cats.Show
  import cats.instances.string._
  import cats.instances.int._
  //import cats.implicits.all._

  val showInt: Show[Int] = Show.apply[Int]
  val showString: Show[String] = Show.apply[String]

  val intAsString: String = showInt.show(123)
  val stringAsString: String = showString.show("abs")

  import cats.syntax.show._

  val showInt2 = 123.show

  import java.util.Date

  implicit val dateShow: Show[Date] =
    Show.show(date => s"${date.getTime}ms since the epoch.")

  implicit val catShow: Show[Cat] =
    Show.show(cat => s"${cat.name} is a ${cat.age} year-old ${cat.color} cat.")

  println(new Date().show)

  println(Cat("Lucky", 13, "white").show)


  import cats.Eq
  import cats.instances.int._

  val eqInt = Eq[Int]

  eqInt.eqv(123, 123)
  eqInt.eqv(123, 124)

  import cats.syntax.eq._
  123 === 123
  132 =!= 342

  import cats.instances.option._
  (Some(2) : Option[Int]) === (None: Option[Int])

  import cats.syntax.option._
  1.some === None
  1.some =!= None


  // Custom types

  import java.util.Date
  import cats.instances.long._

  implicit val dateEq = Eq.instance[Date] { (date1, date2) =>
    date1.getTime === date2.getTime
  }

  val d1 = new Date()
  val d2 = new Date()

  d1 === d2
  d1 === d1

  import cats.instances.int._
  import cats.instances.string._
  import cats.syntax.option._

  implicit val eqCat = Eq.instance[Cat] { (c1, c2) =>
    c1.name === c2.name && c1.age === c2.age && c1.color === c2.color
  }

  val cat1 = Cat("Garfield", 35, "orange and black")
  val cat2 = Cat("Heathcliff", 30, "orange and black")

  val optionCat1 = Option(cat1)
  val optionCat2 = Option.empty[Cat]

  cat1 === cat1
  cat1.some === optionCat1
  optionCat1 =!= optionCat2


}

