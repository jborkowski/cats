package com.jobo

class Monads {

}

import scala.language.higherKinds

trait Monad[F[_]] {
  def pure[A](value: A): F[A]
  def flatMap[A,B](value: F[A])(func: A => F[B]): F[B]
  def map[A,B](value: F[A])(func: A => B): F[B] =
    flatMap(value)(i => pure(func(i)))
}

object MonadLaws {
  // Left identity - f(a) gives same result as pure + flatMap
  // pure(a).flatMap(f) == f(a)

  // Right identity - passing pure to flatMap is the same as doing nothing !
  // m.flatMap(pure) == m

  // Associativity: flatMapping over two functions f and g is the same as flatMapping over f and then flatMapping over g
  // m.flatMap(f).flatMap(g) == m.flatMap(x => f(x).flatMap(g))
}

object CatsMonad extends App {
  import cats.Monad
  import cats.instances.option._
  import cats.instances.list._

  val opt1 = Monad[Option].pure(3)

  val opt2 = Monad[Option].flatMap(opt1)(a => Some(a + 2))

  val opt3 = Monad[Option].map(opt2)(a => 100 * a)

  val list1 = Monad[List].pure(2)

  val list2 = Monad[List].flatMap(list1)(a => List(a, a * 10))

  val list3 = Monad[List].map(list2)(_ + 123)

  // Monad for Future - ExecutionContext (doesn't accept implicit) workaround
  import cats.instances.future._
  import scala.concurrent._
  import scala.concurrent.duration._
  import scala.concurrent.ExecutionContext.Implicits.global

  val fm = Monad[Future]

  Await.result(
    fm.flatMap(fm.pure(1)) { x =>
      fm.pure(x + 2)
    },
    1.second
  )

  // syntax
  // Syntax comes from three places Applicative, Functor and FlatMap
  import cats.syntax.applicative._
  import cats.instances.option._
  import cats.instances.list._

  1.pure[Option]

  1.pure[List]

}

object Identity {
  import cats.Id
  import cats.{Monad => MonadCats}

  val a = MonadCats[Id].pure(3)

  val b = MonadCats[Id].flatMap(a)(_ + 1)

  import cats.syntax.flatMap._
  import cats.syntax.functor._

  for {
    x <- a
    y <- b
  } yield x + y

  def pure[A](value: A): Id[A] = value
  def map[A,B](value: Id[A])(func: A => A): Id[A] =
    func(value)
  def flatMap[A,B](value: Id[A])(func: A => Id[B]): Id[B] =
    func(value)
}

object MSumSquare {
  import scala.language.higherKinds
  import cats.{Monad => MonadCats}
  import cats.syntax.functor._
  import cats.syntax.flatMap._

  def sumSquare[M[_] : MonadCats](a: M[Int], b: M[Int]): M[Int] =
    a.flatMap(x => b.map(y => x*x + y*y))

  import cats.instances.option._
  import cats.instances.list._

  sumSquare(Option(3), Option(3))

  sumSquare(List(1,2,3), List(4,5))

  // OR

  def sumSquareCats[M[_] : MonadCats](a: M[Int], b: M[Int]): M[Int] =
    for {
      x <- a
      y <- b
    } yield x*x + y*y

  sumSquareCats(Option(3), Option(3))

  sumSquareCats(List(1,2,3), List(4,5))


  import cats.Id

  sumSquareCats(3: Id[Int], 4: Id[Int])
}