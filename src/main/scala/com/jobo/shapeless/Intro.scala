package com.jobo.shapeless

import com.jobo.shapeless.AutomaticDerivingType.{iceCreams, writeCsv}

case class Employee(name: String, number: Int, manager: Boolean)
case class IceCream(name: String, numCherries: Int, inCone: Boolean)

object Intro {
  import shapeless._

  val genericEmployee = Generic[Employee].to(Employee("Jonatan", 123, false))

  val genericIceCream = Generic[IceCream].to(IceCream("Sundea", 1, false))

  def genericCsv(gen: String :: Int :: Boolean :: HNil): List[String] =
    List(gen(0).toString, gen(1).toString, gen(2).toString)

  genericCsv(genericEmployee)
  genericCsv(genericIceCream)
}

object HeterogeneousList {
  import shapeless.{HList, ::, HNil}

  val product: String :: Int :: Boolean :: HNil = "Sunday" :: 1 :: false :: HNil

  val first: String = product.head
  val second: Int = product.tail.head
  val rest = product.tail.tail


}

object Gen {
  import shapeless.Generic

  val iceCreamGen = Generic[IceCream]

  val iceCream = IceCream("~Sunae", 1, false)

  val repr = iceCreamGen.to(iceCream)

  val iceCream2 = iceCreamGen.from(repr)
}

sealed trait Shape
final case class Rectangle(width: Double, height: Double) extends Shape
final case class Circle(radius: Double) extends Shape

object GenCoProduct {
  import shapeless.{Coproduct, :+:, CNil, Inl, Inr}

  case class Red()
  case class Amber()
  case class Green()

  type Light = Red :+: Amber :+: Green :+: CNil

  val red: Light = Inl(Red())

  val green: Light = Inr(Inr(Inl(Green())))

  import shapeless.Generic

  val genShape = Generic[Shape]

  genShape.to(Rectangle(3.0 ,5.0))
  genShape.to(Circle(3.4))
}

trait CsvEncoder[A] {
  def encode(value: A): List[String]
}

object CsvEncoder {
  def apply[A](implicit enc: CsvEncoder[A]): CsvEncoder[A] =
    enc

  def instance[A](f: A => List[String]): CsvEncoder[A] =
    new CsvEncoder[A] {
      def encode(value: A): List[String] = f(value)
    }
}

object AutomaticDerivingType {


  implicit val employeeEncoder: CsvEncoder[Employee] =
    new CsvEncoder[Employee] {
      def encode(e: Employee): List[String] =
        List(
          e.name,
          e.number.toString,
          if (e.manager) "yes" else "no"
        )
    }

  implicit val iceCreamEncoder: CsvEncoder[IceCream] =
    (i: IceCream) =>
      List(
        i.name,
        i.numCherries.toString,
        if(i.inCone) "yes" else "no"
      )


  def writeCsv[A](values: List[A])(implicit enc: CsvEncoder[A]): String =
    values.map(v => enc.encode(v).mkString(",")).mkString("\n")

  //test

  val employees: List[Employee] = List(
    Employee("Bill", 1, true),
    Employee("Peter", 2, false),
    Employee("Milton", 3, true)
  )

  println(writeCsv(employees))

  //Prove for all ADT
  val iceCreams: List[IceCream] = List(
    IceCream("Bambo", 0, false),
    IceCream("Waf", 3, true)
  )

  println(writeCsv(iceCreams))

  // Pair encoder

  implicit def pairEncoder[A,B](implicit
                                aEncoder: CsvEncoder[A],
                                bEncoder: CsvEncoder[B]
                               ): CsvEncoder[(A,B)] = {
    case (a,b) =>
      aEncoder.encode(a) ++ bEncoder.encode(b)
  }

  writeCsv(employees zip iceCreams)

  CsvEncoder[IceCream] // CsvEncoder

  implicitly[CsvEncoder[IceCream]]

  import shapeless._
  import CsvEncoder._

  the[CsvEncoder[IceCream]]

  implicit val booleanEnc: CsvEncoder[Boolean] =
    instance((b: Boolean) => if(b) List("yes") else List("no"))

}

object DerivingType extends App {

  // Deriving Instances of product
  def createEncoder[A](f: A => List[String]): CsvEncoder[A] =
    (value: A) => f(value)

  implicit val stringEncoder: CsvEncoder[String] =
    createEncoder(str => List(str))

  implicit val booleanEncoder: CsvEncoder[Boolean] =
    createEncoder(b => List(if(b) "yes" else "no"))

  implicit val intEncoder: CsvEncoder[Int] =
    createEncoder(i => List(i.toString))

  import shapeless.{HList, ::, HNil}

  implicit val hnilEncoder: CsvEncoder[HNil] =
    createEncoder(hnil => Nil)

  import shapeless.Lazy

  implicit def hlistEncoder[H, T <: HList](implicit
                                           hEncoder: Lazy[CsvEncoder[H]],
                                           tEncoder: CsvEncoder[T]): CsvEncoder[H :: T] =
    createEncoder {
      case h :: t =>
        hEncoder.value.encode(h) ++ tEncoder.encode(t)
    }

  val reprEncoder: CsvEncoder[String :: Int :: Boolean :: HNil] =
    implicitly

  reprEncoder.encode("abc" :: 123 :: true :: HNil)

  import shapeless.Generic

  implicit val iceCreamEncoderN: CsvEncoder[IceCream] = {
    val gen = Generic[IceCream]
    val enc = CsvEncoder[gen.Repr]
    createEncoder(iceCream => enc.encode(gen.to(iceCream)))
  }

  val s = writeCsv(iceCreams)

  println(s)

  GenericEnc
}

object GenericEnc {
  import DerivingType._
  import shapeless.Generic
  import shapeless.Lazy

  implicit def genericEncoder[A,R](implicit
                                  //gen: Generic[A] { type Repr = R },
                                  gen: Generic.Aux[A,R],
                                  enc: Lazy[CsvEncoder[R]]
                                  ): CsvEncoder[A] =
    createEncoder(a => enc.value.encode(gen.to(a)))

  val employees: List[Employee] = List(
    Employee("Bill", 1, true),
    Employee("Peter", 2, false),
    Employee("Milton", 3, true)
  )

  println(writeCsv(employees))
}

object DerivingInstancesForCoproduct {
  import DerivingType._
  import GenericEnc._
  import shapeless.Lazy
  import shapeless.{Coproduct, :+:, CNil, Inl, Inr, ::, HNil}


  implicit val cnilEncoder: CsvEncoder[CNil] =
    createEncoder(cnil => throw new Exception("Inconceivable!"))

  implicit def coproductEncoder[H, T <: Coproduct](implicit
                                                   hEncoder: Lazy[CsvEncoder[H]],
                                                   tEncoder: CsvEncoder[T]): CsvEncoder[H :+: T] = createEncoder {
      case Inl(h) => hEncoder.value.encode(h)
      case Inr(t) => tEncoder.encode(t)
    }

  implicit val doubleEncoder: CsvEncoder[Double] =
    createEncoder(d => List(d.toString))

  val shapes: List[Shape] = List(
    Rectangle(3.0, 4.0),
    Circle(1.0)
  )

  writeCsv(shapes)

  //Binary tree - Recursive

  sealed trait Tree[A]
  case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]
  case class Leaf[A](value: A) extends Tree[A]


  // LAZY - WORKAROUND FOR RECURSIVE

  CsvEncoder[Tree[Int]]


}